package net.epitech.java.td01.td0ioc.model.types;

public enum SexType {
	MALE, FEMALE, UNKNOWN;
}
