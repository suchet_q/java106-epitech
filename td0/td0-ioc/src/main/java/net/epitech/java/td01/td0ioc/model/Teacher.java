package net.epitech.java.td01.td0ioc.model;

import net.epitech.java.td01.td0ioc.model.contract.GenreIdentifiable;
import net.epitech.java.td01.td0ioc.model.contract.Nameable;
import net.epitech.java.td01.td0ioc.model.types.SexType;

public class Teacher implements Nameable, GenreIdentifiable {

	private String name;
	private SexType sex;

	/**
	 * Create a teacher entity
	 * 
	 * @param name
	 *            full name of the teacher
	 * @param sex
	 */
	public Teacher(String name, SexType sex) {
		this.name = name;
		this.sex = sex;
	}

	public String getName() {
		return this.name;
	}

	public SexType getSex() {

		return this.sex;
	}

}
