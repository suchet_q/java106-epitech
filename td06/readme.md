#�TD6 : Persitance dans un contexte WEB #


## Objectifs du TD ##


### Rappels ###

+ R�cap du TD5 

### Techniques ###

+ jndi
+ Les pools de connection Tomcat
+ spring data JPA
+ QueryDSL et les repository
+ logs



JNDI
---------

* C'est l'oppos� de l'injection de d�pendance: les resources sont r�cup�r�e par le client (alors que dans ICO, c'est le conteneur qui va injecter les resources dans le code)
* the Java Naming and Directory Interface (JNDI) is a Java API for a directory service that allows Java software clients to discover and look up data and objects via a name.

Les pools de connections Tomcat
----------------------------------

* dans les exemples pr�c�dent, qui �tait responsable de l'ouverture de la connection?
* comment faire pour emp�cher d'avoir trop de connections ouvertes simultan�ments?
* Que ce passe-t-il si on d�ploie plusieurs application web qui consomment simultan�ment des ressources?
* r�ponse: utilisation de pool de connection
> a connection pool is a cache of database connections maintained so that the connections can be reused when future requests to the database are required. Connection pools are used to enhance the performance of executing commands on a database.

####�Les frameworks de pool de connection ####

* [C3PO](http://sourceforge.net/projects/c3p0/)
* Apache DBCP
* [BoneCP](http://jolbox.com/)



#### Les pools de connection avec Apache Tomcat ####

On utilise les outils de la fondation Apache

* Commons DBCP
* Commons Pool
* [Documentation Tomcat](http://tomcat.apache.org/tomcat-7.0-doc/jndi-datasource-examples-howto.html#Database_Connection_Pool_%28DBCP%29_Configurations)


------------------------
    <Context>[1]
    <Resource name="jdbc/TestDB" auth="Container" type="javax.sql.DataSource"
                    maxActive="100" maxIdle="30" maxWait="10000"
                    username="javauser" password="javadude" driverClassName="com.mysql.jdbc.Driver"
                    url="jdbc:mysql://localhost:3306/javatest"/>
    </Context>
  
-----------------------
* ici la resource JNDI est "jdbc/TestDB
* elle doit �tre reprise dans votre descripteur de serlet (web.xml)
* Il faut que les (jar de mysql)[http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.29.tar.gz] soient dans le r�pertoire lib de votre Tomcat

Spring DATA
----------------

### Pr�sentation du projet

* (Voir le site De spring data)[http://projects.spring.io/spring-data/]

----------------
Makes it easy to use *new data access technologies*, such as non-relational databases, map-reduce frameworks, and cloud based data services. Spring Data also provides *improved support for relational database technologies*. This is an umbrella project which contains many subprojects that are specific to a given database. 

-----------------

### Comment l'inclure dans un projet spring ?

#### D�pendance Maven

---------------------
    	<dependency>
    			<groupId>org.springframework.data</groupId>
    			<artifactId>spring-data-jpa</artifactId>
    			<version>1.4.3.RELEASE</version>
    		</dependency>

---------------------

#### DAO vs REPOSITORY

* ce sont deux pattern d'architecture (!= design patterns)

##### DAO

    In computer software, a data access object (DAO) is an object that 
    provides an abstract interface to some type of database or other 
    persistence mechanism.
    
#####�Repository

    A Repository mediates between the domain and data mapping layers,
    acting like an in-memory domain object collection. Client objects
    construct query specifications declaratively and submit them to 
    Repository for satisfaction. Objects can be added to and removed
    from the Repository, as they can from a simple collection of
    objects, and the mapping code encapsulated by the Repository will
     carry out the appropriate operations behind the scenes 

##### Pour r�sumer

* Repository est plus abstrait (plus �loign� de la DB) que le DAO
* il permet de r�duire significativement la duplicaiton de code
* il facilite les tests

#### Comment l'utiliser?

D�clarer simplement des interfaces dans un sous package.

----------------------

    package net.epitech.java.td.repositories;
    import java.util.List;
    import net.epitech.java.td.domain.Course;
    import org.springframework.data.jpa.repository.JpaRepository;
    import org.springframework.data.querydsl.QueryDslPredicateExecutor;
    public interface CourseRepository extends JpaRepository<Course, Integer>,
    { ... }

-----------------------


#### Comment le configurer ?

Utiliser votre classe de configuration (@Configuration) et ajouter les annotations:

-------------------------
@EnableJpaRepositories(basePackages = "net.epitech.java.td.repositories")
@EnableTransactionManagement

-------------------------

ajouter ensuite les beans correspondants (voir net.epitech.java.td.config.PersistenceConfiguration)

### Une solution plus simple que JPA?

Avoir directement les repository vous �vite de cr�e et d'impl�menter les DAO, mais vous �tes toujours oblig� d'avoir du JPQL/Criteria dans votre couche service.

#### QueryDSL

Query DSL vous permet, en �crivant vos interface, d'avoir directement les requ�tes cr�ees. Pour cela, il suffit d'extends 
    
    org.springframework.data.querydsl.QueryDslPredicateExecutor<?>
    
puis de d�clarer des fonctions comme:

    public List<Course> findCourseByName(String name);
    
l'impl�mentation de la fonction sera automatiquement inject�e dans votre service.

#### Les meta objets

On peut utiliser une API criteria plus simple que JPA avec QueryDSL. Il faut g�n�rer les meta objets lors de la compilation, avec le plugin maven apt

        <plugin>
          <groupId>com.mysema.maven</groupId>
          <artifactId>maven-apt-plugin</artifactId>
          <version>1.0.4</version>
          <executions>
            <execution>
              <phase>generate-sources</phase>
              <goals>
                <goal>process</goal>
              </goals>
              <configuration>

                <outputDirectory>target/generated-sources</outputDirectory>
                <!-- States that the APT code generator should look for JPA annotations -->
                <processor>com.mysema.query.apt.jpa.JPAAnnotationProcessor</processor>
              </configuration>
            </execution>
          </executions>
        </plugin>


Il faut �galement ajouter le code g�n�r� dans eclipse.

        <groupId>org.codehaus.mojo</groupId>
              <artifactId>build-helper-maven-plugin</artifactId>
              <version>1.8</version>
              <executions>
                <execution>
                  <id>add-source</id>
                  <phase>generate-sources</phase>
                  <goals>
                    <goal>add-source</goal>
                  </goals>
                  <configuration>
                    <sources>
                      <source>${project.build.directory}/generated-sources</source>
                    </sources>
                  </configuration>
                </execution>
              </executions>
            </plugin>



Les Logs
==================

* *Ne JAMAIS utiliser system.out.println dans un contexte WEB*
* Tous les frameworks que nous utilisons utilisent d�j� un m�canisme de log qu'il convient d'adopter

logback
---------

* successeur de log4j
* se configure � l'aide d'un fichier XML

### exemple

---------------------------------
        <?xml version="1.0" encoding="UTF-8"?>
        <configuration>
          <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
            <encoder>
              <pattern>%d %5p | %t | %-55logger{55} | %m %n</pattern>
            </encoder>
          </appender>
          <root>
            <level value="INFO"/>
            <appender-ref ref="CONSOLE"/>
          </root>
        </configuration>

-------------------------------------

### faire taire spring et hibernate

-------------------------------------
          <logger name="org.springframework">
            <level value="INFO"/>
          </logger>
          <logger name="org.hibernate">
            <level value="INFO"/>
          </logger>
  
--------------------------------------

Int�gration Maven
-------------------


    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <version>1.7.1</version>
    </dependency>
    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
      <version>1.1.1</version>
    </dependency>
    
    

A vous de jouer!
==================
* td06 hands on

Rappels des besoins
-------------------

* 1 cours = 1 prof + 1 horaire
* un cours est fait pendant une plage horaire (toute les semaines) (eg. JAVA tous les lundi de 15h � 18h)
* deux cours ne peuvent pas se chevaucher
* lors de la cr�ation d'un nouveau cours, syst�me doit proposer 3 horaires disponibles sur la semaine dans la tranche des Lundi-Vendredi ; 9h-12h30 14h-18h. Si aucun des 3 horaires ne convient, le syst�me doit afficher 3 autres horaires et ainsi de suite.

* Pages � cr�er: 
* /cours qui affiche la liste des cours avec leurs horaires et les prof associ�s
* /profs qui affiche la liste des prof avec leurs cours associ�s. Il affiche l'emploi du temps d'un prof
* /nouveau-cours qui affiche un formulaire qui permet noter le nom du cours, le prof associ� (dans la liste des profs), et qui propose de choisir l'horaire du cours dans une liste.







